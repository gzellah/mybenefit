

$(document).ready(function(){
  $('.first_squash').on("click", function () {
    console.log($(this).parent().find("a").attr("href"));
    window.location = $(this).parent().find("a").attr("href");
  })
})

window.onscroll = myFunction;

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
  console.log("myFunction");
}

// localization
var arrLang = {
  "pl-pl": {
    "DLA NIEJ": "DLA NIEJ",
    "DLA NAS": "DLA NAS",
    "DLA MNIE": "DLA MNIE",
    "DLA PRZYJACIÓL": "DLA PRZYJACIÓL",
    "DLA NIEGO": "DLA NIEGO",
    "Walentynkowe Prezenty": "Walentynkowe Prezenty",
    "Od serce": "Od serce kafetrii",
    "Niespodzianki": "Już za 2 dni kolejne niespodzianki!",
    "Limanova": "Limanova Hotel wSPAniały prezent",
    "Grape": "Grape Hotel & Restaurant Wrocław",
    "Douglas":"Douglas",
    "Masaz": "Masaż relaksacyjny 60min Narodziny Piękna Wrocław",
    "Zapisz": "Zapisz się",
    "Kruk": "W. KRUK (kody o podwyższonej wartości)",
    "Dwojga": "Masaż dla Dwojga",
    "Termy": "Bania Thermal Baths",
    "Walentykowo": "S: WALENTYNKOWY SHOW REWIOWY LOVE STORY - 14.02 - Warszawa (bileteria MB)",
    "Klasyczne": "Masaż klasyczny pleców WorkFit Warszawa",
    "Wakacje": "Wakacje w Polsce W górach",
    "Morze": "Wakacje w Polsce Nad morzem",
    "Tourism": "Tourism for me",
    "Shops": "Shops for me",
    "Massage": "Massage for me",
    "Edukacja": "Edukacja dla niej",
    "Subscribe": "Zapisz się do newslettera, by jako pierwszy otrzymywać kolejne nasze propozycje!",
    "Wspolne": "Wspólne zainteresowania, pasje, plany i... wspólny prezent? Dlaczego nie! Serca zakochanych biją w jednym tempie - niech nikt inny nie próbuje ich doganiać!",
    "Posluchaj": " Posłuchaj Serca Kafeterii i w Walentynki podaruj bliskiej osobie więcej miłosci ! Przygotowalismy zestawienie prezentowych bestsellerów dla niej, dla niego dla Was i... dla Ciebie - miłość  nie jedno ma imię, zastanawiałes się już kogo obdarujesz w tym roku",
    "Serce bije": "Serce Kafeterii bije w rytmie kobiet! Posłuchaj jego rad i podaruj jej walentynkowy prezent, który wzrusza i cieszy. Gwarantujemy, że będzie zadowolona!  "

  },
  "en-gb": {
    "DLA NIEJ": "FOR HER",
    "DLA NAS": "FOR US",
    "DLA MNIE": "FOR ME",
    "DLA PRZYJACIÓL": "FOR MY FRIENDS",
    "DLA NIEGO": "FOR HIS",
    "Walentynkowe Prezenty": "Valentine's Gifts",
    "Od serce": "FROM THE BOTTOM OF ONE’S HEART",
    "Niespodzianki": "More inspiration on January 28!",
    "Limanova": "Limanowa Hotel Wonderful time in SPA",
    "Kruk": "W. KRUK (codes with increased value) ",
    "Grape": "Grape Hotel & Restaurant Wrocław",
    "Douglas":"Douglas",
    "Zapisz": "Sign up",
    "Tourism": "Tourism for me",
    "Shops": "Shops for me",
    "Massage": "Massage for me",
    "Recreation": "Recreation for me",
    "Culture": "Culture",
    "Walentykowo": "S: VALENTINE'S SHOW LOVE STORY – 14/02 - Warsaw (MB ticket office)",
    "Mistycznia": "K: Mistyczne Źródło– 17/02 - Wrocław (MB ticket office)",
    "Termy": "Termy Bania",
    "Wakacje": "Holidays in Poland In the mountains ",
    "Dwojga": "Massage for two",
    "Morze": "Holidays in Poland At the seaside",
    "Wspolne":"Common interests, passions, plans and ... a shared gift? Why not! The hearts of lovers beat at one pace - let no one else try to catch up!",
    "Masaz": "60-minute relaxation massage  Narodziny Piękna Wroclaw",
    "Klasyczne": "Classic back massage WorkFit Warsaw",
    "Subscribe": "Subscribe to the newsletter and be the first to know all the gift ideas!",
    "Posluchaj": "Listen to the Heart of Cafeteria and on Valentine's Day give to your loved one even more love! We have prepared a set of bestsellers for her, for him, for you both and ... for you - love has not got a name, have you already wondered to whom are you giving this year?",
    "Serce bije": "The heart of Cafeteria beats in the rhythm of women! Listen to his advice and give her a moving and enjoyable Valentine gift. We guarantee that she will be satisfied!"
  }
};

// The default language is English
var lang = "pl-pl";
// Check for localStorage support
if('localStorage' in window){

   var usrLang = localStorage.getItem('uiLang');
   if(usrLang) {
       lang = usrLang
   }

}


console.log(lang);

        $(document).ready(function() {

          $(".lang").each(function(index, element) {
            $(this).text(arrLang[lang][$(this).attr("key")]);
          });
        });

        // get/set the selected language
        $(".translate").click(function() {
          var lang = $(this).attr("id");

          // update localStorage key
          if('localStorage' in window){
               localStorage.setItem('uiLang', lang);
               console.log( localStorage.getItem('uiLang') );
          }

          $(".lang").each(function(index, element) {
            $(this).text(arrLang[lang][$(this).attr("key")]);
          });
        });
